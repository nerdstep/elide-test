
INSERT INTO ALPHA (id, name) VALUES ('a1', 'alpha1');
INSERT INTO ALPHA (id, name) VALUES ('a2', 'alpha2');
INSERT INTO ALPHA (id, name) VALUES ('a3', 'alpha3');

INSERT INTO BRAVO (id, name) VALUES ('b1', 'bravo1');
INSERT INTO BRAVO (id, name) VALUES ('b2', 'bravo2');
INSERT INTO BRAVO (id, name) VALUES ('b3', 'bravo3');

INSERT INTO RELATIONSHIP (from_id, to_id) VALUES ('a1', 'a2');
INSERT INTO RELATIONSHIP (from_id, to_id) VALUES ('a1', 'a3');
INSERT INTO RELATIONSHIP (from_id, to_id) VALUES ('a1', 'b1');
INSERT INTO RELATIONSHIP (from_id, to_id) VALUES ('a1', 'b2');

INSERT INTO RELATIONSHIP (from_id, to_id) VALUES ('a2', 'a3');
INSERT INTO RELATIONSHIP (from_id, to_id) VALUES ('a2', 'b1');
INSERT INTO RELATIONSHIP (from_id, to_id) VALUES ('a2', 'b3');

INSERT INTO RELATIONSHIP (from_id, to_id) VALUES ('a3', 'a1');
INSERT INTO RELATIONSHIP (from_id, to_id) VALUES ('a3', 'b3');