package example.domain;

import com.yahoo.elide.annotation.Include;
import com.yahoo.elide.annotation.SharePermission;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Setter
@SharePermission
@NoArgsConstructor
@Table(name = "BRAVO")
@Include(rootLevel = true, type = "bravos")
public class Bravo implements Serializable {

    private String id;
    private String name;
    private Set<Alpha> alphas = new HashSet<>();
    private Set<Bravo> bravos = new HashSet<>();

    @Id
    @Column(name = "ID", unique = true, nullable = false)
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(
            name = "RELATIONSHIP",
            joinColumns = @JoinColumn(name = "FROM_ID"),
            inverseJoinColumns = @JoinColumn(name = "TO_ID")
    )
    public Set<Alpha> getAlphas() {
        return alphas;
    }

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(
            name = "RELATIONSHIP",
            joinColumns = @JoinColumn(name = "FROM_ID"),
            inverseJoinColumns = @JoinColumn(name = "TO_ID")
    )
    public Set<Bravo> getBravos() {
        return bravos;
    }
    
}
